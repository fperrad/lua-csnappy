
# lua-csnappy

---

## Overview

lua-csnappy is a binding of the __csnappy__ library
which implements the Google's Snappy (de)compressor.

Snappy uses a [LZ77](https://en.wikipedia.org/wiki/LZ77_and_LZ78)-type
algorithm with a fixed, byte-oriented encoding.

## References

The __csnappy__ library is available
at <https://github.com/zeevt/csnappy>.

The specification and the original C++ implementation of Snappy are available
at <https://google.github.io/snappy/>.

## Status

lua-csnappy is in beta stage.

It's developed for Lua 5.1, 5.2, 5.3 & 5.4.

## Download

lua-csnappy source can be downloaded from
[Framagit](https://framagit.org/fperrad/lua-csnappy).

## Installation

lua-csnappy is available via LuaRocks:

```sh
luarocks install lua-csnappy
```

or manually, with:

```sh
make install
```

## Test

The test suite requires the module
[lua-TestMore](https://fperrad.frama.io/lua-TestMore/).

    make test

## Copyright and License

Copyright &copy; 2012-2018 Fran&ccedil;ois Perrad

This library is licensed under the terms of the BSD license,
like csnappy & Snappy.
