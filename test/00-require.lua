#!/usr/bin/env lua

require 'Test.Assertion'

plan(7)

if not require_ok 'snappy' then
    BAIL_OUT "no lib"
end

local m = require 'snappy'
is_table( m )
equals( m, package.loaded.snappy )

matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'compressor', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

