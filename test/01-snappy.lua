#!/usr/bin/env lua

require 'Test.Assertion'

plan(23)

local snappy = require 'snappy'

error_matches(function () snappy.compress(snappy) end,
        "bad argument #1 to 'compress' %(string expected, got table%)",
        "bad argument")

error_matches(function () snappy.decompress(snappy) end,
        "bad argument #1 to 'decompress' %(string expected, got table%)",
        "bad argument")

error_matches(function () snappy.decompress '' end,
        "snappy: bad header",
        "bad header")

error_matches(function () snappy.decompress 'malformed' end,
        "snappy: malformed data",
        "malformed data")

local input = ''
local compressed = snappy.compress(input)
is_string(compressed, "empty string")
equals(#compressed, 1)
local decompressed = snappy.decompress(compressed)
equals(decompressed, input)

local len = 1
for _ = 0, 15 do
    input = string.rep('0', len)
    compressed = snappy.compress(input)
    decompressed = snappy.decompress(compressed)
    equals(decompressed, input, "length: " .. tostring(len))
    len = len * 3
end
