
version_pl := \
use strict; \
use warnings; \
while (<>) { \
    next unless m{(\d\.\d\.\d)}; \
    print $$1; \
};

ifeq ($(wildcard bed),bed)
LUA     := $(CURDIR)/bed/bin/lua
LUACHECK:= $(CURDIR)/bed/bin/luacheck
LUAROCKS:= $(CURDIR)/bed/bin/luarocks
LUACOV  := $(CURDIR)/bed/bin/luacov
LUACOV_C:= $(CURDIR)/bed/bin/luacov-console
else
LUA     := lua
LUACHECK:= luacheck
LUAROCKS:= luarocks
LUACOV  := luacov
LUACOV_C:= luacov-console
endif
VERSION := $(shell perl -e '$(version_pl)' < lsnappy.c)
TARBALL := lua-csnappy-$(VERSION).tar.gz
REV     := 1

LUAVER  := 5.3
PREFIX  := /usr/local
DPREFIX := $(DESTDIR)$(PREFIX)
LIBDIR  := $(DPREFIX)/lib/lua/$(LUAVER)
INSTALL := install

ifeq ($(wildcard bed),bed)
INCS    := -I $(CURDIR)/bed/include
else
INCS    := -I /usr/include/lua$(LUAVER)
endif
DEFS    :=
WARN    := -Wall -pedantic
CFLAGS  := $(INCS) $(DEFS) $(WARN) -O2 -fPIC $(GCOVFLAGS)
LDFLAGS := -shared

BED_OPTS:= --lua latest

all: snappy.so

csnappy/csnappy_compress.c csnappy/csnappy_decompress.c:
	git submodule update --init

lsnappy.o: csnappy/csnappy_compress.c csnappy/csnappy_decompress.c

snappy.so: lsnappy.o
	$(CC) $(LDFLAGS) -o $@ $<

install:
	$(INSTALL) -m 755 -D snappy.so          $(LIBDIR)/snappy.so

uninstall:
	rm -f $(LIBDIR)/snappy.so

manifest_pl := \
use strict; \
use warnings; \
my @files = qw{ \
    MANIFEST \
    csnappy/csnappy.h \
    csnappy/csnappy_compat.h \
    csnappy/csnappy_internal.h \
    csnappy/csnappy_internal_userspace.h \
    csnappy/csnappy_compress.c \
    csnappy/csnappy_decompress.c \
}; \
while (<>) { \
    chomp; \
    next if m{^\.}; \
    next if m{^debian/}; \
    next if m{^rockspec/}; \
    next if m{^csnappy$$}; \
    push @files, $$_; \
} \
print join qq{\n}, sort @files;

rockspec_pl := \
use strict; \
use warnings; \
use Digest::MD5; \
open my $$FH, q{<}, q{$(TARBALL)} \
    or die qq{Cannot open $(TARBALL) ($$!)}; \
binmode $$FH; \
my %config = ( \
    version => q{$(VERSION)}, \
    rev     => q{$(REV)}, \
    md5     => Digest::MD5->new->addfile($$FH)->hexdigest(), \
); \
close $$FH; \
while (<>) { \
    s{@(\w+)@}{$$config{$$1}}g; \
    print; \
}

version:
	@echo $(VERSION)

CHANGES:
	perl -i.bak -pe "s{^$(VERSION).*}{q{$(VERSION)  }.localtime()}e" CHANGES

tag:
	git tag -a -m 'tag release $(VERSION)' $(VERSION)

MANIFEST:
	git ls-files | perl -e '$(manifest_pl)' > MANIFEST

$(TARBALL): MANIFEST
	[ -d lua-csnappy-$(VERSION) ] || ln -s . lua-csnappy-$(VERSION)
	perl -ne 'print qq{lua-csnappy-$(VERSION)/$$_};' MANIFEST | \
	    tar -zc -T - -f $(TARBALL)
	rm lua-csnappy-$(VERSION)

dist: $(TARBALL)

rockspec: $(TARBALL)
	perl -e '$(rockspec_pl)' rockspec.in > rockspec/lua-csnappy-$(VERSION)-$(REV).rockspec

rock:
	$(LUAROCKS) pack rockspec/lua-csnappy-$(VERSION)-$(REV).rockspec

deb:
	echo "lua-csnappy ($(shell git describe --dirty)) unstable; urgency=medium" >  debian/changelog
	echo ""                         >> debian/changelog
	echo "  * UNRELEASED"           >> debian/changelog
	echo ""                         >> debian/changelog
	echo " -- $(shell git config --get user.name) <$(shell git config --get user.email)>  $(shell date -R)" >> debian/changelog
	fakeroot debian/rules clean binary

bed:
	hererocks bed $(BED_OPTS) --no-readline --luarocks latest --verbose
	bed/bin/luarocks install lua-testassertion
	bed/bin/luarocks install luacheck
	bed/bin/luarocks install luacov
	bed/bin/luarocks install luacov-console
	bed/bin/luarocks install luacov-reporter-lcov
	hererocks bed --show
	bed/bin/luarocks list

check: test

test:
	prove --exec=$(LUA) test/*.lua

luacheck:
	$(LUACHECK) --std=min --config .test.luacheckrc test

coverage:
	rm -f luacov.*
	-prove --exec="$(LUA) -lluacov" test/*.lua
	$(LUACOV_C) test
	$(LUACOV_C) -s test

lcov:
	$(LUACOV) -r lcov
	genhtml luacov.report.out -o public/

README.html: README.md
	Markdown.pl README.md > README.html

pages:
	mkdocs build -d public

clean:
	rm -f MANIFEST *.so *.o *.bak README.html

realclean: clean
	rm -rf bed

.PHONY: test rockspec deb CHANGES

