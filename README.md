
lua-csnappy
===========

Introduction
------------

lua-csnappy is a binding of the `csnappy` library which implements the Google's Snappy (de)compressor.

Snappy uses a [LZ77](https://en.wikipedia.org/wiki/LZ77_and_LZ78)-type algorithm with a fixed, byte-oriented encoding.

The `csnappy` library is available at <https://github.com/zeevt/csnappy>.

The specification and the original C++ implementation of Snappy are available at <https://google.github.io/snappy/>.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-csnappy/>,
and the sources are hosted at <https://framagit.org/fperrad/lua-csnappy>.

Copyright and License
---------------------

Copyright (c) 2012-2018 Francois Perrad

This library is licensed under the terms of the BSD license, like csnappy & Snappy.

